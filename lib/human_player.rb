class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "where would you like to mark?"
    move = gets.chomp
    move.split(", ").map(&:to_i)
  end

  def display(board)
    board_display = board.grid.each_with_index.reduce("") do |board_str, (row, i)|

      row_str = (i == 0 ? "" : "---" + ("|---" * row.length) + "\n")

      row_str = row.each_with_index.reduce(row_str) do |row, (mark, i)|
        mark = (mark == :X || mark == :O ? mark : " ")
        cell_str = (i == 0 ? "" : "|") + " #{mark} "
        row + cell_str
      end

      board_str + "\n" + row_str

    end

    puts board_display
  end
end
