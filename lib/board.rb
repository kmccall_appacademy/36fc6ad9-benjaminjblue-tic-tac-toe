class Board
  attr_reader :grid
  def initialize(grid = [[nil,nil,nil], [nil,nil,nil], [nil,nil,nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end

  def winner
    [:X,:O].each do |mark|
      horizontal = @grid.reduce(false) do |verdict, row|
        verdict || row.all? {|cell| cell == mark}
      end
      vertical = @grid.transpose.reduce(false) do |verdict, row|
        verdict || row.all? {|cell| cell == mark}
      end
      #00 11 22
      #0last 1last -1 #until last0
      reversed = @grid.reverse
      diagonal = (0...@grid.length).all? {|i| @grid[i][i] == mark} ||
                 (0...@grid.length).all? {|i| reversed[i][i] == mark}

      return mark if horizontal || vertical || diagonal
    end
    nil
  end

  def over?
    winner || @grid.all?{|row| row.all? {|cell| cell == :X || cell == :O}}
  end
end
