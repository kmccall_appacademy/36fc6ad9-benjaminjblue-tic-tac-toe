class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    empty_spaces = []
    (0...@board.grid.length)
    @board.grid.each_with_index do |row, i|
      (0...row.length).each do |j|
        if @board.empty?([i,j])
          empty_spaces.push([i,j])
        end
      end
    end
    empty_spaces.each do |pos|
      grid = @board.grid.map{|row| [].replace(row)}
      grid[pos[0]][pos[1]] = :O
      if winner(grid) == @mark
        return pos
      end
    end
    empty_spaces[rand(empty_spaces.length)]
  end

  private

  def winner(grid)
    [:X,:O].each do |mark|
      horizontal = grid.reduce(false) do |verdict, row|
        verdict || row.all? {|cell| cell == mark}
      end
      vertical = grid.transpose.reduce(false) do |verdict, row|
        verdict || row.all? {|cell| cell == mark}
      end
      #00 11 22
      #0last 1last -1 #until last0
      reversed = grid.reverse
      diagonal = (0...grid.length).all? {|i| grid[i][i] == mark} ||
                 (0...grid.length).all? {|i| reversed[i][i] == mark}

      return mark if horizontal || vertical || diagonal
    end
    nil
  end

end
